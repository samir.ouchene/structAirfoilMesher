!  This file is part of structAirfoilMesher.

!  structAirfoilMesher is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.

!  structAirfoilMesher is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.

!  You should have received a copy of the GNU General Public License
!  along with structAirfoilMesher.  If not, see <http://www.gnu.org/licenses/>.

!  author: Konstantinos Diakakis 

module vtkOutput

    implicit none

contains

    subroutine writeGridVtk(grid, griddim, nplanes, mgrd_lvls, deltplane, asweep, name)

        Use vardef, only: srf_grid_type

        type(srf_grid_type), intent(in) :: grid
        integer, intent(in) :: griddim, nplanes, mgrd_lvls
        double precision, intent(in) :: deltplane, asweep
        character(300), intent(in) :: name

        integer i, j, k
        integer imax_tot, jmax_tot, kmax_tot
        integer imax, jmax, kmax
        integer lvl, step
        logical threed

        double precision :: angle, delta_x

        character filename*64
        character levelstr*6

        threed = .false.
        if (griddim==3 .and. nplanes>1) threed = .true.

        imax_tot = grid%imax
        jmax_tot = grid%jmax
        if (threed) then
            kmax_tot = nplanes
        else
            kmax_tot = 1
        end if

        if (mgrd_lvls.gt.0) then
            if (mod(imax_tot - 1, 2**mgrd_lvls).ne.0) then
                write (*, *) 'Grid imax cannot be divided by multigrid levels.'
                stop
            elseif (mod(jmax_tot - 1, 2**mgrd_lvls).ne.0) then
                write (*, *) 'Grid jmax cannot be divided by multigrid levels.'
                stop
            elseif ((mod(nplanes - 1, 2**mgrd_lvls).ne.0) .and. (threed)) then
                write (*, *) 'Grid kmax cannot be divided by multigrid levels.'
                stop
            end if
        end if

        do lvl = 0, mgrd_lvls

            if (mgrd_lvls.gt.0) then
                write (*, *) 'Multigrid level: ', lvl

                write (levelstr, '(a5,i1)') 'level', lvl
                filename = trim(name)//'_'//trim(levelstr)//'.vtk'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            else
                filename = trim(name)//'.vtk'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            end if

            imax = (imax_tot - 1)/2**lvl + 1
            jmax = (jmax_tot - 1)/2**lvl + 1
            if (threed) then
                kmax = (nplanes - 1)/2**lvl + 1
            else
                kmax = 1
            end if

            open (1, file=trim(filename))
            step = 2**(lvl)

            ! Write header to output file

            write (1, '("# vtk DataFile Version 2.0")')
            if (mgrd_lvls.gt.0) write (1, *) trim(name)//" Structured Mesh - Multigrid "//trim(levelstr)
            if (mgrd_lvls.eq.0) write (1, *) trim(name)//" Structured Mesh"
            write (1, *) 'ASCII'
            write (1, *) 'DATASET STRUCTURED_GRID'
            if (threed) then
                write (1, *) 'DIMENSIONS ', imax, jmax, kmax
                write (1, *) 'POINTS ', imax*jmax*kmax, ' Double'
            else
                write (1, *) 'DIMENSIONS ', imax, jmax
                write (1, *) 'POINTS ', imax*jmax, ' Double'
            end if

            ! Write out grid - in reverse i direction to preserve positive volumes

            if (threed) then

                do k = 1, kmax
                    if (asweep.gt.0.d0) then
                        angle = asweep*4.d0*atan(1.d0)/180.d0
                        delta_x = tan(angle)*dble((k - 1)*step)*deltplane
                    else
                        delta_x = 0.d0
                    end if
                    do j = 1, jmax
                        do i = imax, 1, -1
                            write (1, '(3(es17.8,1x))') grid%x((i - 1)*step + 1, (j - 1)*step + 1) + delta_x, &
                                grid%y((i - 1)*step + 1, (j - 1)*step + 1), &
                                dble((k - 1)*step)*deltplane
                        end do
                    end do
                end do

            else

                do j = 1, jmax
                    do i = imax, 1, -1
                        write (1, '(2(es17.8,1x))') grid%x((i - 1)*step + 1, (j - 1)*step + 1), &
                            grid%y((i - 1)*step + 1, (j - 1)*step + 1)
                    end do
                end do

            end if

            close (1)

        end do

    end subroutine writeGridVtk

end module vtkOutput
